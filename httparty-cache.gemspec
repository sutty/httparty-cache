# frozen_string_literal: true

Gem::Specification.new do |spec|
  spec.name          = 'httparty-cache'
  spec.version       = '0.0.6'
  spec.authors       = %w[f]
  spec.email         = %w[f@sutty.nl]

  spec.summary       = 'HTTParty cache'
  spec.description   = 'A cache manager for HTTParty'
  spec.homepage      = "https://0xacab.org/sutty/#{spec.name}"
  spec.license       = 'MIT'

  spec.required_ruby_version = Gem::Requirement.new('>= 2.7')

  spec.metadata = {
    'bug_tracker_uri' => "#{spec.homepage}/issues",
    'homepage_uri' => spec.homepage,
    'source_code_uri' => spec.homepage,
    'changelog_uri' => "#{spec.homepage}/-/blob/master/CHANGELOG.md",
    'documentation_uri' => "https://rubydoc.info/gems/#{spec.name}"
  }

  spec.files         = Dir['lib/**/*']
  spec.require_paths = %w[lib]

  spec.extra_rdoc_files = Dir['README.md', 'CHANGELOG.md', 'LICENSE.txt']
  spec.rdoc_options += [
    '--title', "#{spec.name} - #{spec.summary}",
    '--main', 'README.md',
    '--line-numbers',
    '--inline-source',
    '--quiet'
  ]

  spec.add_runtime_dependency 'httparty', '~> 0.18'

  spec.add_development_dependency 'activesupport'
  spec.add_development_dependency 'factory_bot'
  spec.add_development_dependency 'pry'
  spec.add_development_dependency 'rspec', '~> 3.6', '>= 3.6.0'
  spec.add_development_dependency 'rspec-tap-formatters'
  spec.add_development_dependency 'yard'

  spec.add_development_dependency 'hiredis-client', '~> 0.14.0'
  spec.add_development_dependency 'jekyll'
  spec.add_development_dependency 'redis-client', '~> 0.14.0'
  spec.add_development_dependency 'rubocop'
  spec.add_development_dependency 'timecop'
  spec.add_development_dependency 'webmock', '~> 1.24', '>= 1.24.3'
end
