# frozen_string_literal: true

require 'spec_helper'
require 'httparty/cache/store/jekyll'
require 'httparty/cache/store/memory'
require 'httparty/cache/store/redis'

RSpec.describe HTTParty::Cache do
  before do
    @url = 'https://social.distributed.press/'
    @klass = Class.new
    @klass.instance_eval do
      include HTTParty
      include HTTParty::Cache
    end

    @cache_stores = %i[memory redis jekyll]

    ::Jekyll::Cache.cache_dir = Dir.mktmpdir
  end

  def random_query
    rand(10).times.map do
      [SecureRandom.hex(2), SecureRandom.hex(2)]
    end.to_h
  end

  describe '.caching' do
    it 'sets caching' do
      @klass.caching(caching = [true, false].sample)

      expect(@klass.default_options[:caching]).to(eql(caching))
    end
  end

  describe '.cache_store' do
    it 'sets a cache store' do
      @klass.cache_store(cache_store = [true, false].sample)

      expect(@klass.default_options[:cache_store]).to(eql(cache_store))
    end
  end

  describe '.get' do
    it 'defaults to no caching' do
      body = 'OK'

      stub_request(:get, @url)
        .to_return do |_request|
          {
            status: 200,
            body: body
          }
        end

      stub_request(:head, @url)
        .to_return do |_request|
          body = 'NOTOK'

          { status: 200 }
        end

      expect(@klass.get(@url).body).to(eql('OK'))
      expect(@klass.get(@url).body).to(eql('OK'))
    end

    it 'behaves the same with caching off' do
      body = 'OK'

      stub_request(:get, @url)
        .to_return do |_request|
          {
            status: 200,
            body: body
          }
        end

      stub_request(:head, @url)
        .to_return do |_request|
          body = 'NOTOK'

          { status: 200 }
        end

      @klass.caching false
      expect(@klass.get(@url).body).to(eql('OK'))
      expect(@klass.get(@url).body).to(eql('OK'))
    end

    it "doesn't cache if the server doesn't allow caching" do
      @klass.caching true
      @cache_stores.each do |cache_store|
        @klass.cache_store cache_store
        @klass.cache_store.clear

        ok = 'OK'
        stub_request(:get, @url)
          .to_return do |_request|
            {
              status: 200,
              body: ok
            }
          end

        stub_request(:head, @url)
          .to_return do |_request|
            ok = 'CHANGED'
            { status: 200 }
          end

        first = @klass.get(@url)
        last = @klass.get(@url)

        expect(first.class).to(eql(last.class))
        expect(first.cache_key).to(eql(last.cache_key))
        expect(first.body).not_to(eql(last.body))
      end
    end

    it 'caches requests when repeated' do
      @klass.caching true
      @cache_stores.each do |cache_store|
        @klass.cache_store cache_store
        @klass.cache_store.clear

        stub_request(:get, @url)
          .to_return do |_request|
            {
              status: 200,
              body: 'OK',
              headers: {
                'Cache-Control': 'no-cache'
              }
            }
          end

        stub_request(:head, @url)
          .to_return do |_request|
            { status: 304 }
          end

        first = @klass.get(@url)
        last = @klass.get(@url)

        expect(first.class).to(eql(last.class))
        expect(first.cache_key).to(eql(last.cache_key))
        expect(first.body).to(eql(last.body))
        expect(first.revalidate?).to(eql(true))
        expect(first.stale?).to(eql(false))
      end
    end

    it 'caches requests with blocks but evaluates them once' do
      @klass.caching true
      @cache_stores.each do |cache_store|
        @klass.cache_store cache_store
        @klass.cache_store.clear

        stub_request(:get, @url)
          .to_return do |_request|
            {
              status: 200,
              body: 'OK',
              headers: {
                'Cache-Control': 'no-cache'
              }
            }
          end

        stub_request(:head, @url)
          .to_return do |_request|
            { status: 304 }
          end

        count = nil
        fragment = nil
        first =
          @klass.get(@url) do |f|
            count = 1
            fragment = f
          end

        expect(fragment.class).to(eql(HTTParty::ResponseFragment))

        last =
          @klass.get(@url) do |fragment|
            count = 2
            fragment = nil
          end

        expect(count).to(eql(1))
        expect(fragment.class).to(eql(HTTParty::ResponseFragment))
        expect(first.body).to(eql(last.body))
      end
    end

    it 'revalidates requests with blocks' do
      @klass.caching true
      @cache_stores.each do |cache_store|
        @klass.cache_store cache_store
        @klass.cache_store.clear

        body = 'OK'
        stub_request(:get, @url)
          .to_return do |_request|
            {
              status: 200,
              body: body,
              headers: {
                'Cache-Control': 'no-cache'
              }
            }
          end

        stub_request(:head, @url)
          .to_return do |_request|
            body = 'REVALIDATED'
            { status: 200 }
          end

        count = nil
        fragment = nil
        first =
          @klass.get(@url) do |f|
            count = 1
            fragment = f
          end

        expect(fragment.class).to(eql(HTTParty::ResponseFragment))

        last =
          @klass.get(@url) do |fragment|
            count = 2
            fragment = nil
          end

        expect(count).to(eql(2))
        expect(fragment.class).to(eql(HTTParty::ResponseFragment))
        expect(first.body).not_to(eql(last.body))
      end
    end

    it 'revalidates requests and gets latest body' do
      @klass.caching true
      @cache_stores.each do |cache_store|
        @klass.cache_store cache_store
        @klass.cache_store.clear

        body = SecureRandom.hex

        stub_request(:get, @url)
          .to_return do |_request|
            {
              status: 200,
              body: body,
              headers: {
                'Cache-Control': 'no-cache'
              }
            }
          end

        stub_request(:head, @url)
          .to_return do |_request|
            body = SecureRandom.hex

            { status: 200 }
          end

        expect(@klass.get(@url).body).not_to(eql(@klass.get(@url).body))
      end
    end

    it 'revalidates requests and gets cached response if not modified' do
      @klass.caching true
      @cache_stores.each do |cache_store|
        @klass.cache_store cache_store
        @klass.cache_store.clear

        body = original_body = SecureRandom.hex

        stub_request(:get, @url)
          .to_return do |_request|
            {
              status: 200,
              body: body,
              headers: {
                'Cache-Control': 'no-cache'
              }
            }
          end

        stub_request(:head, @url)
          .to_return do |_request|
            body = SecureRandom.hex

            { status: 304 }
          end

        expect(@klass.get(@url).body).to(eql(@klass.get(@url).body))

        expect(body).not_to(eql(@klass.get(@url).body))
        expect(original_body).to(eql(@klass.get(@url).body))
      end
    end

    it 'caches requests with query strings when repeated' do
      @klass.caching true
      @cache_stores.each do |cache_store|
        @klass.cache_store cache_store
        @klass.cache_store.clear

        query = random_query
        query2 = random_query

        stub_request(:get, "#{@url}?#{URI.encode_www_form query}")
          .to_return do |_request|
            {
              status: 200,
              body: 'OK',
              headers: {
                'Cache-Control': 'no-cache'
              }
            }
          end

        stub_request(:head, "#{@url}?#{URI.encode_www_form query}")
          .to_return do |_request|
            {
              status: 200,
              headers: {
                'Cache-Control': 'no-cache'
              }
            }
          end

        stub_request(:get, "#{@url}?#{URI.encode_www_form query2}")
          .to_return do |_request|
            {
              status: 200,
              body: 'NOTOK',
              headers: {
                'Cache-Control': 'no-cache'
              }
            }
          end

        expect(@klass.get(@url, query: query).body).to(eql(@klass.get(@url, query: query).body))
      end
    end

    it 'revalidates requests with query strings and gets latest body' do
      @klass.caching true
      @cache_stores.each do |cache_store|
        @klass.cache_store cache_store
        @klass.cache_store.clear

        query = random_query
        body = SecureRandom.hex
        url = "#{@url}?#{URI.encode_www_form query}"

        stub_request(:get, url)
          .to_return do |_request|
            {
              status: 200,
              body: body,
              headers: {
                'Cache-Control': 'no-cache'
              }
            }
          end

        stub_request(:head, url)
          .to_return do |_request|
            body = SecureRandom.hex

            { status: 200 }
          end

        expect(@klass.get(@url, query: query).body).not_to(eql(@klass.get(@url, query: query).body))
      end
    end

    it 'revalidates requests with query strings and gets cached response if not modified' do
      @klass.caching true
      @cache_stores.each do |cache_store|
        @klass.cache_store cache_store
        @klass.cache_store.clear

        query = random_query
        body = original_body = SecureRandom.hex
        url = "#{@url}?#{URI.encode_www_form query}"

        stub_request(:get, url)
          .to_return do |_request|
            {
              status: 200,
              body: body,
              headers: {
                'Cache-Control': 'no-cache'
              }
            }
          end

        stub_request(:head, url)
          .to_return do |_request|
            body = SecureRandom.hex

            { status: 304 }
          end

        expect(@klass.get(@url, query: query).body).to(eql(@klass.get(@url, query: query).body))

        expect(body).not_to(eql(@klass.get(@url, query: query).body))
        expect(original_body).to(eql(@klass.get(@url, query: query).body))
      end
    end

    it 'revalidates requests when etag changes' do
      @klass.caching true
      @cache_stores.each do |cache_store|
        @klass.cache_store cache_store
        @klass.cache_store.clear
        body = SecureRandom.hex
        etag = original_etag = SecureRandom.hex

        stub_request(:get, @url)
          .to_return do |_request|
            {
              status: 200,
              body: body,
              headers: {
                'Cache-Control': 'no-cache',
                'ETag': etag
              }
            }
          end

        stub_request(:head, @url)
          .to_return do |request|
            body = SecureRandom.hex
            etag = SecureRandom.hex

            expect(request.headers['If-None-Match']).to(eql(original_etag))

            original_etag = etag

            { status: 200 }
          end

        expect(@klass.get(@url).body).not_to(eql(@klass.get(@url).body))
      end
    end

    it 'revalidates requests when etag doesnt change' do
      @klass.caching true
      @cache_stores.each do |cache_store|
        @klass.cache_store cache_store
        @klass.cache_store.clear

        body = original_body = SecureRandom.hex
        etag = SecureRandom.hex

        stub_request(:get, @url)
          .to_return do |_request|
            {
              status: 200,
              body: body,
              headers: {
                'Cache-Control': 'no-cache',
                'ETag': etag
              }
            }
          end

        stub_request(:head, @url)
          .to_return do |request|
            body = SecureRandom.hex

            expect(request.headers['If-None-Match']).not_to(eql(nil))

            { status: 304 }
          end

        expect(@klass.get(@url).body).to(eql(@klass.get(@url).body))

        expect(body).not_to(eql(@klass.get(@url).body))
        expect(original_body).to(eql(@klass.get(@url).body))
      end
    end

    it 'receives a custom cache key' do
      stub_request(:get, @url)
        .to_return do |_|
          { status: 200 }
        end

      headers = {
        HTTParty::Request::SENSITIVE_HEADERS.sample => SecureRandom.hex,
        'X-Check' => SecureRandom.hex
      }

      expect(
        @klass
          .get(@url, headers: headers, cache_key: @url)
          .request
          .cache_params
          .keys
      ).to(eql(%w[x-cache-key]))
    end

    it 'removes sensitive headers from cache key' do
      stub_request(:get, @url)
        .to_return do |_|
          { status: 200 }
        end

      headers = {
        HTTParty::Request::SENSITIVE_HEADERS.sample => SecureRandom.hex,
        'X-Check' => SecureRandom.hex
      }

      expect(
        @klass
          .get(@url, headers: headers)
          .request
          .cache_params
          .keys
      ).to(eql(%w[x-check]))
    end
  end
end
