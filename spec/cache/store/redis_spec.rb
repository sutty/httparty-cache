# frozen_string_literal: true

require 'spec_helper'
require 'httparty/cache/store/redis'

klass = HTTParty::Cache::Store::Redis

RSpec.describe klass do
  describe '#new' do
    it 'initializes' do
      expect do
        klass.new
      end.not_to(raise_error(RedisClient::CannotConnectError), ENV['REDIS_URL'])
    end
  end

  describe '#set' do
    it 'sets a value' do
      key = SecureRandom.hex
      val = SecureRandom.hex

      store = klass.new

      expect(store.set(key, val)).to(eql(val))
    end
  end

  describe '#get' do
    it 'gets a value after it is set' do
      key = SecureRandom.hex
      val = SecureRandom.hex

      store = klass.new

      expect(store.get(key)).to(eql(nil))

      store.set(key, val)

      expect(store.get(key)).to(eql(val))
    end
  end

  describe '#getset' do
    it 'sets a value and returns it' do
      key = SecureRandom.hex
      val = 0
      store = klass.new

      rand(100).times do
        expect(
          store.getset(key) do
            val += 1
          end
        ).to(eql(1))
      end
    end
  end

  describe '#key?' do
    it 'tells you if a key exists' do
      store = klass.new

      expect(store.key?(SecureRandom.hex)).to(eql(false))
    end

    it 'tells you if a key exists after is set' do
      store = klass.new
      key = SecureRandom.hex

      store.set(key, SecureRandom.hex)
      expect(store.key?(key)).to(eql(true))
    end
  end
end
