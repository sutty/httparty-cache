# frozen_string_literal: true

require 'spec_helper'
require 'httparty/cache/response'

RSpec.describe HTTParty::Cache::Response do
  before do
    @url = 'https://social.distributed.press/'
    @klass = Class.new
    @klass.instance_eval do
      include HTTParty
      include HTTParty::Cache
    end
  end

  describe '#dump' do
    it 'can be dumped' do
      stub_request(:get, @url)
        .to_return do |_request|
          {
            status: 200,
            body: 'OK'
          }
        end

      expect do
        Marshal.dump(@klass.get(@url))
      end.not_to(raise_error(TypeError))
    end
  end

  describe '#load' do
    it 'can be loaded' do
      stub_request(:get, @url)
        .to_return do |_request|
          {
            status: 200,
            body: 'OK'
          }
        end

      @response = @klass.get(@url)

      expect do
        @response = Marshal.load(Marshal.dump(@response))
      end.not_to(raise_error(TypeError))
    end
  end

  describe '#date' do
    it 'returns a timestamp' do
      date = Time.new 2023, 12, 28, 15, 2, 25, 0

      stub_request(:get, @url)
        .to_return do |_request|
          {
            status: 200,
            body: 'OK',
            headers: {
              Date: 'Thu, 28 Dec 2023 15:02:25 GMT'
            }
          }
        end

      expect(@klass.get(@url).date.class).to(eql(Time))
      expect(@klass.get(@url).date).to(eql(date))
    end

    it 'fails when no date is provided' do
      stub_request(:get, @url)
        .to_return do |_request|
          {
            status: 200,
            body: 'OK'
          }
        end

      expect do
        @klass.get(@url).date
      end.to(raise_error(HTTParty::ResponseError))
    end
  end

  describe '#age' do
    it 'is mostly zero' do
      stub_request(:get, @url)
        .to_return do |_request|
          {
            status: 200,
            body: 'OK'
          }
        end

      expect(@klass.get(@url).age).to(eql(0))
    end

    it 'is the difference between date and now' do
      age = rand(100)

      stub_request(:get, @url)
        .to_return do |_request|
          {
            status: 200,
            body: 'OK',
            headers: {
              Date: (Time.now - age).httpdate
            }
          }
        end

      expect(@klass.get(@url).age).to(eql(age))
    end
  end

  describe '#cache_control' do
    it 'returns an empty hash when header is missing' do
      stub_request(:get, @url)
        .to_return do |_request|
          {
            status: 200,
            body: 'OK'
          }
        end

      expect(@klass.get(@url).cache_control.class).to(eql(Hash))
      expect(@klass.get(@url).cache_control.empty?).to(eql(true))
    end

    it 'parses the header' do
      max_age = rand(100)

      stub_request(:get, @url)
        .to_return do |_request|
          {
            status: 200,
            body: 'OK',
            headers: {
              'Cache-Control': "max-age=#{max_age},no-store, public"
            }
          }
        end

      expect(@klass.get(@url).cache_control.class).to(eql(Hash))
      expect(@klass.get(@url).cache_control[:public]).to(eql(true))
      expect(@klass.get(@url).cache_control[:no_store]).to(eql(true))
      expect(@klass.get(@url).cache_control[:max_age]).to(eql(max_age))
    end

    it 'is case insensitive' do
      max_age = rand(100)

      stub_request(:get, @url)
        .to_return do |_request|
          {
            status: 200,
            body: 'OK',
            headers: {
              'Cache-Control': "MaX-aGe=#{max_age},nO-sTorE, pUblic"
            }
          }
        end

      expect(@klass.get(@url).cache_control.class).to(eql(Hash))
      expect(@klass.get(@url).cache_control[:public]).to(eql(true))
      expect(@klass.get(@url).cache_control[:no_store]).to(eql(true))
      expect(@klass.get(@url).cache_control[:max_age]).to(eql(max_age))
    end
  end

  describe '#stale?' do
    it 'is false when the age is 0' do
      max_age = rand(100)

      stub_request(:get, @url)
        .to_return do |_request|
          {
            status: 200,
            body: 'OK',
            headers: {
              'Cache-Control': "max-age=#{max_age}"
            }
          }
        end

      expect(@klass.get(@url).stale?).to(eql(false))
    end

    it 'is false when the age when max-age is not set' do
      stub_request(:get, @url)
        .to_return do |_request|
          {
            status: 200,
            body: 'OK',
            headers: {
              Date: (Time.now - 180).httpdate
            }
          }
        end

      expect(@klass.get(@url).stale?).to(eql(false))
    end

    it 'is true when the age is greater than max-age' do
      max_age = rand(100)
      age = rand(101..200)

      stub_request(:get, @url)
        .to_return do |_request|
          {
            status: 200,
            body: 'OK',
            headers: {
              Date: (Time.now - age).httpdate,
              'Cache-Control': "max-age=#{max_age}"
            }
          }
        end

      expect(@klass.get(@url).stale?).to(eql(true))
    end
  end

  describe '#revalidate?' do
    it 'is true if cache control is missing' do
      stub_request(:get, @url)
        .to_return do |_request|
          {
            status: 200,
            body: 'OK'
          }
        end

      expect(@klass.get(@url).revalidate?).to(eql(true))
    end

    it 'is true if cache control is no store' do
      stub_request(:get, @url)
        .to_return do |_request|
          {
            status: 200,
            body: 'OK',
            headers: {
              'Cache-Control': 'no-store'
            }
          }
        end

      expect(@klass.get(@url).revalidate?).to(eql(true))
    end

    it 'is true if no-cache is provided' do
      stub_request(:get, @url)
        .to_return do |_request|
          {
            status: 200,
            body: 'OK',
            headers: {
              'Cache-Control': 'no-cache'
            }
          }
        end

      expect(@klass.get(@url).revalidate?).to(eql(true))
    end

    it 'is true if max-age=0 and must-revalidate are provided' do
      stub_request(:get, @url)
        .to_return do |_request|
          {
            status: 200,
            body: 'OK',
            headers: {
              'Cache-Control': %w[max-age=0 must-revalidate].shuffle
            }
          }
        end

      expect(@klass.get(@url).revalidate?).to(eql(true))
    end
  end

  describe '#vary' do
    it 'returns empty hash when missing' do
      stub_request(:get, @url)
        .to_return do |_request|
          {
            status: 200,
            body: 'OK'
          }
        end

      expect(@klass.get(@url).vary).to(eql({}))
    end

    it 'returns a hash of request headers' do
      stub_request(:get, @url)
        .to_return do |_request|
          {
            status: 200,
            body: 'OK',
            headers: {
              Vary: 'Accept'
            }
          }
        end

      expect(@klass.get(@url, headers: { Accept: 'application/json' }).vary['accept']).to(eql('application/json'))
    end

    it 'sorts headers' do
      stub_request(:get, @url)
        .to_return do |_request|
          {
            status: 200,
            body: 'OK',
            headers: {
              Vary: 'Cookies, Accept'
            }
          }
        end

      cookie = SecureRandom.hex
      expect(
        @klass.get(@url, headers: { Accept: 'application/json', Cookies: cookie }).vary.values
      ).to(eql(
             @klass.get(@url, headers: { Cookies: cookie, Accept: 'application/json' }).vary.values
           ))
    end
  end

  describe '#cache_key' do
    it 'returns a cache key based on uri' do
      stub_request(:get, @url)
        .to_return do |_request|
          {
            status: 200,
            body: 'OK',
            headers: {
              Vary: 'Accept'
            }
          }
        end

      expect(@klass.get(@url,
                        headers: { Accept: 'application/json' }).cache_key).to(eql("httparty-cache:response:ad37b16165bd104d6ea205967ebe40dcc707af185994b5491c56e3d532a5f625"))
    end

    it 'returns different cache keys per vary header' do
      stub_request(:get, @url)
        .to_return do |_request|
          {
            status: 200,
            body: 'OK',
            headers: {
              Vary: 'Accept'
            }
          }
        end

      expect(@klass.get(@url,
                        headers: { Accept: 'application/json' }).cache_key).not_to(eql(@klass.get(@url).cache_key))
    end
  end
end
