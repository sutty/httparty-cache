# frozen_string_literal: true

module HTTParty
  # HTTParty plugin to manage cache
  module Cache
    def self.included(base)
      base.extend ClassMethods

      require_relative 'cache/response'
      require_relative 'cache/request'
    end

    module ClassMethods
      # This is both a setter and a getter...
      #
      # @param :cache_store [Symbol,HTTParty::Cache::Store::Abstract]
      # @return [HTTParty::Cache::Store::Abstract]
      def cache_store(cache_store = nil)
        case cache_store
        when NilClass
          return default_options[:cache_store]
        when Symbol
          require_relative "cache/store/#{cache_store}"

          class_name = cache_store.to_s.split('_').map(&:capitalize).join
          cache_store = Object.const_get("HTTParty::Cache::Store::#{class_name}").new
        end

        default_options[:cache_store] = cache_store
      end

      # Toggle caching
      #
      # @param :caching [Bool]
      # @return [Bool]
      def caching(caching)
        default_options[:caching] = caching
      end

      # Sets cache_control
      def cache_control(cache_control)
        default_options[:cache_control] = cache_control
      end
    end
  end
end
