# frozen_string_literal: true

require_relative 'abstract'

module HTTParty
  module Cache
    module Store
      class Memory < Abstract
        extend Forwardable

        attr_reader :store

        def_delegator :@store, :[], :get
        def_delegator :@store, :[]=, :set

        def_delegators :@store, :key?, :delete

        def initialize(**)
          clear
        end

        def _dump(_)
          Marshal.dump(nil)
        end

        def self._load(_)
          new
        end

        def getset(key)
          store[key] ||= yield
        end

        def clear
          @store = {}
          nil
        end
      end
    end
  end
end
