# frozen_string_literal: true

module HTTParty
  module Cache
    module Store
      # Cache store
      class Abstract
        # @return [Hash]
        attr_reader :options

        # @param :key [String]
        # @return [Response,nil]
        def get(key)
          raise NotImplementedError
        end

        # @param :key [String]
        # @param :response [Response]
        # @return [Response]
        def set(key, response)
          raise NotImplementedError
        end

        # @param :key [String]
        # @return nil
        def delete(key)
          raise NotImplementedError
        end

        # @param :key [String]
        # @yield
        def getset(key, &block)
          raise NotImplementedError
        end

        # @param :key [String]
        # @return [Boolean]
        def key?(key)
          raise NotImplementedError
        end

        def clear
          raise NotImplementedError
        end

        def marshal_dump
          [
            self.class.name,
            options || {}
          ]
        end

        def marshal_load(array)
          Object.const_get(array.first).new(**array.last)
        end
      end
    end
  end
end
