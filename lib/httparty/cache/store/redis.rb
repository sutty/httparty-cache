# frozen_string_literal: true

require_relative 'abstract'
require 'redis-client'

begin
  require 'hiredis-client'
  RedisClient.default_driver = :hiredis
rescue LoadError
end

module HTTParty
  module Cache
    module Store
      class Redis < Abstract
        # A Marshaled `nil` value
        NIL = "\x04\b0"

        # @return [Hash]
        attr_reader :options

        # @param :redis_url [String]
        def initialize(redis_url: nil)
          marshal_load(redis_url)
        end

        def marshal_dump
          options[:redis_url]
        end

        def marshal_load(redis_url)
          @options = {}
          @options[:redis_url] = redis_url || ENV['REDIS_URL']
          @config = RedisClient.config(url: @options[:redis_url])
          @redis = @config.new_pool(size: 2)
          @redis.call('PING')
        end

        # I couldn't get this to work! It's never called even if I dump
        # the store directly
        def _dump(level)
          options[:redis_url]
        end

        def self._load(string)
          new(redis_url: string)
        end

        # Retrieves a key
        #
        # @param :key [String]
        # @return [Response,nil]
        def get(key)
          Marshal.load(redis('GET', key) || NIL)
        end

        # Stores a Marshaled value indexed by a key.
        #
        # @param :key [String]
        # @param :response [Response]
        # @return [Response]
        def set(key, response)
          redis('SET', key, Marshal.dump(response))
          response
        end

        # Returns a key or set its value to a block result
        #
        # @param :key [String]
        # @return [Response,nil]
        def getset(key)
          return get(key) if key?(key)

          set(key, yield)
        end

        # Removes a key from Redis
        #
        # @param :key [String]
        # @return [nil]
        def delete(key)
          redis('DEL', key)
          nil
        end

        # Discovers if a key exists
        #
        # @param :key [String]
        # @return [Boolean]
        def key?(key)
          redis('EXISTS', key).positive?
        end

        def clear
          redis('FLUSHALL', 'SYNC')
          nil
        end

        private

        # Execs Redis commands and collects stats
        def redis(*args)
          @redis.call(*args)
        end
      end
    end
  end
end
