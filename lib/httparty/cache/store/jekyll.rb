# frozen_string_literal: true

require_relative 'abstract'
require 'jekyll/cache'

module HTTParty
  module Cache
    module Store
      class Jekyll < Abstract
        extend Forwardable

        def_delegators :@store, :key?, :delete, :clear, :getset

        # @return [Hash]
        attr_reader :options

        # @param :name [String]
        def initialize(name: self.class.name)
          @options = {}
          @options[:name] = name
          @store = ::Jekyll::Cache.new(name)
        end

        def _dump(_)
          Marshal.dump(options[:name])
        end

        def self._load(string)
          new(name: Marshal.load(string))
        end

        def set(key, response)
          @store[key] = response
          response
        end

        def get(key)
          @store[key]
        rescue RuntimeError
          nil
        end
      end
    end
  end
end
