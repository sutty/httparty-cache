# frozen_string_literal: true

require 'digest/sha2'

module HTTParty
  module Cache
    # Extends HTTParty::Request with caching methods
    #
    # @see {https://developer.mozilla.org/en-US/docs/Web/HTTP/Caching}
    module Request
      SENSITIVE_HEADERS = %w[cookie authorization date signature].freeze
      CACHE_REVALIDATION_HEADERS = {
        'ETag' => 'If-None-Match',
        'Last-Modified' => 'If-Modified-Since'
      }.freeze

      # Calculates a cache key from URI + query string + headers
      #
      # @return [String]
      def cache_key
        @cache_key ||=
          begin
            key = "#{uri}##{URI.encode_www_form(cache_params)}"
            "httpparty-cache:request:#{Digest::SHA2.hexdigest(key)}"
          end
      end

      # @return [Hash]
      def cache_params
        return { 'x-cache-key' => options[:cache_key] } if options.key? :cache_key

        options[:headers].to_h.transform_keys(&:downcase).except(*SENSITIVE_HEADERS)
      end

      def validate
        super

        raise ArgumentError, 'Caching is enabled but store was not provided' if caching? && cache_store.nil?
      end

      # A cacheable request
      #
      # @return [Boolean]
      def cacheable?
        caching? && http_method == Net::HTTP::Get
      end

      # Patch HTTParty::Request#perform to provide caching for GET
      # requests
      #
      # @return [HTTParty::Response]
      def perform(&block)
        return super(&block) unless cacheable?

        if response_cached?
          handle_revalidation(&block)
        else
          super(&block).tap do |response|
            # Point current request to cached response
            cache_store.set(response.cache_key, response)
            cache_store.set(cache_key, response.cache_key)
          end
        end
      end

      # When the request is performed for the first time or has been
      # evicted from cache or is corrupted
      #
      # @return [Bool]
      def response_cached?
        cache_store.key?(cache_key) &&
          cache_store.key?(cache_store.get(cache_key)) &&
          cached_response.is_a?(HTTParty::Response)
      end

      def caching?
        !!options[:caching]
      end

      # @return [HTTParty::Response]
      def cached_response
        @cached_response ||= cache_store.get(cache_store.get(cache_key))
      end

      private

      # Revalidates request
      #
      # @return [HTTParty::Response]
      def handle_revalidation(&block)
        if cached_response.revalidate? || cached_response.stale?
          self.http_method = Net::HTTP::Head

          add_cache_revalidation_headers

          # Don't run the block here
          response = perform

          if response.not_modified?
            cached_response.headers['date'] = response.headers['date']
            response = cached_response
            response.hit!
          else
            self.http_method = Net::HTTP::Get
            # Remove from cache so we can perform the request, otherwise
            # we produce an infinite loop
            cache_store.delete(cache_key)
            @cached_response = response = perform(&block)
            response.miss!
          end

          cache_store.set(response.cache_key, response)
          response
        else
          cached_response
        end
      end

      # Just a shortcut
      def cache_store
        options[:cache_store]
      end

      # Adds revalidation headers unless they're already present
      #
      # @return [nil]
      def add_cache_revalidation_headers
        options[:headers] ||= {}

        CACHE_REVALIDATION_HEADERS.each_pair do |original_header, revalidation_header|
          next if (value = cached_response.headers[original_header]).nil?

          options[:headers][revalidation_header] ||= value
        end
      end
    end
  end
end

# Patch HTTParty::Request
HTTParty::Request.class_eval do
  prepend HTTParty::Cache::Request
end
