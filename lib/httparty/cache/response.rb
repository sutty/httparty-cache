# frozen_string_literal: true

require 'digest/sha2'

module HTTParty
  module Cache
    # Extends HTTParty::Response with caching methods
    #
    # @see {https://developer.mozilla.org/en-US/docs/Web/HTTP/Caching}
    module Response
      X_HEADER = 'x-httparty-cache'
      HIT = 'hit'
      MISS = 'miss'

      # Prevent stack too deep errors while keeping important
      # information
      def _dump(_level)
        _request = HTTParty::Request.new(request.http_method, request.path, headers: request.options[:headers]&.to_hash)
        _body = response.instance_variable_get(:@body)
        _read_adapter = _body.is_a?(Net::ReadAdapter)

        response.instance_variable_set(:@body, nil) if _read_adapter

        Marshal.dump([_request, response, parsed_response, body]).tap do
          response.instance_variable_set(:@body, _body) if _read_adapter
        end
      end

      def hit!
        response[X_HEADER] = headers[X_HEADER] = HIT
      end

      def miss!
        response[X_HEADER] = headers[X_HEADER] = MISS
      end

      def fresh!
        response.delete(X_HEADER)
        headers.delete(X_HEADER)
      end

      def hit?
        headers[X_HEADER] == HIT
      end

      def miss?
        headers[X_HEADER] == MISS
      end

      def reused?
        headers.key?(X_HEADER)
      end

      # Parses Cache-Control headers
      #
      # @see {https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/cache-control}
      # @return [Hash]
      def cache_control
        @cache_control ||= headers['Cache-Control'].to_s.downcase.split(',').map(&:strip).map do |c|
          k, v = c.split('=', 2).map(&:strip)

          [k.tr('-', '_').to_sym, (v ? Integer(v) : true)]
        end.to_h
      end

      # Returns the request date
      #
      # @return [Time]
      def date
        Time.parse(headers['date'])
      rescue TypeError, ArgumentError
        raise HTTParty::ResponseError, response
      end

      # Returns the current age of the request in seconds
      #
      # @return [Integer]
      def age
        (Time.now - date).to_i
      rescue HTTParty::ResponseError
        0
      end

      # The response is stale when it has been cached for more than the
      # allowed max age
      #
      # @return [Bool]
      def stale?
        !cache_control[:max_age].nil? && age > cache_control[:max_age]
      end

      # Detects if the response must be revalidated even if still fresh.
      #
      # @see {https://developer.mozilla.org/en-US/docs/Web/HTTP/Caching#force_revalidation}
      # @return [Bool]
      def revalidate?
        cache_control.empty? ||
          cache_control[:no_store] ||
          cache_control[:no_cache] ||
          (
            cache_control[:max_age]&.zero? &&
            cache_control[:must_revalidate]
          )
      end

      # Returns the request headers that vary responses
      #
      # @return [Hash]
      def vary
        @vary ||= request.options[:headers].to_h.slice(*headers['vary'].to_s.split(',').map(&:strip).sort).transform_keys(&:downcase)
      end

      # Generates a cache key
      #
      # @return [String]
      def cache_key
        @cache_key ||=
          begin
            key = "#{uri}##{URI.encode_www_form(vary)}"
            "httparty-cache:response:#{Digest::SHA2.hexdigest(key)}"
          end
      end
    end
  end
end

# Patch HTTParty::Response
HTTParty::Response.prepend HTTParty::Cache::Response
